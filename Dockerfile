FROM node:9-slim
WORKDIR /src/server
COPY package.json /src/server
RUN npm install
COPY . /app
CMD [ "npm", "start" ]
