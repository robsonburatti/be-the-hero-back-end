import React from 'react';

export default function Header({ children }) {
    // const { title } = props;

    return (
        <header>
            {/* <h1>{ title }</h1> */}
            {/* <h1>{props.title}</h1> */}
            <h1>{ children }</h1>
        </header>
    );
}
